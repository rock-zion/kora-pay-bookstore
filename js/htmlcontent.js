export function renderCarouselCells(book) {
  return `<div class="carousel-cell">
    <span id="carousel-more-container" class="carousel-more-container">
     <img id="carousel-more" src="https://res.cloudinary.com/rock-zion/image/upload/v1619690543/Korapay/more.svg" alt=""/>
    </span>
    <img class="carousel-book-cover" src=${
      book.image
    } alt="" /><div id="cell-details-overlay" class="cell-details-overlay"> <img class="close-overlay" id="close-overlay" src="https://res.cloudinary.com/rock-zion/image/upload/v1619690539/Korapay/close-details.svg" alt="" /> <div class="display-1"><span class="regular-12 ${
    book.status === "Available" ? "books-status-green" : "books-status-red"
  }">${book.status}</span><h5 class="bold-18 carousel-title">${
    book.book_title
  }</h5> <div class="book-author-published df"> <span> <span class="book-subtext regular-12 authors">${
    book.authors
  }</span>${
    book.published !== null
      ? `<span class="book-subtext regular-12">${book.published}
                            </span>`
      : ""
  }</span></div> <h6 class="bold-12 carousel-title-sm di">Genre:&nbsp;
                              ${book.genre
                                .map((item) => {
                                  return `<span class="carousel-category regular-12"> ${item} </span>`;
                                })
                                .join(
                                  ""
                                )}</h6><h6 class="bold-12 carousel-title-sm">Label:&nbsp;
                              ${book.label
                                .map((item) => {
                                  return `<span class="carousel-category regular-12"> ${item} </span>`;
                                })
                                .join("")}</h6></div>
                                <div class="display-3"><div class="carousel-rating-outer"><div class="carousel-rating">
                                <span><h6 class="bold-12 df">Rating:&nbsp;<span class="regular-12">${book.ratings.toFixed(
                                  1
                                )}</span></h6><span class="stars">${renderBookRating(
    book.ratings
  )}</span> </span><div class="vertical-line-dark"></div><div class="shape-numbers-outer">
    <div class="shape-container">
                            <span class="shape people"><img src="https://res.cloudinary.com/rock-zion/image/upload/v1619690539/Korapay/followers-dark.svg" alt="followers icon" /></span>
                            <span class="number regular-12">${
                              book.tagged
                            }</span>
                        </div>
                        <div class="shape-container">
                            <span class="shape likes-dark"><img src="https://res.cloudinary.com/rock-zion/image/upload/v1619690541/Korapay/likes-dark.svg" alt="likes icon" /></span>
                            <span class="number regular-12">${book.likes}</span>
                        </div> </div></div></div></div></div></div>`;
}

export function renderBookCard(book) {
  return `<div class="book-card">
    <div class="book-cover-container">
        <img class="book-cover" src=${book.image} alt="" />
    </div>
    <div class="details">
        <span class="books-status regular-12 ${
          book.status === "Available"
            ? "books-status-green"
            : "books-status-red"
        }">${book.status}</span>
        <h5 class="book-title">${book.book_title}</h5>
        <div class="book-author-published df">
            <span class="book-subtext regular-12 authors">${book.authors}</span>
            ${
              book.published !== null
                ? `<span class="book-subtext regular-12 published">${book.published}
            </span>`
                : ""
            }
        </div>
        <div class="book-card-category">
            ${book.genre
              .map((item) => {
                return `<span class="category regular-12"> ${item} </span>`;
              })
              .join("")}
        </div>
        <div class="ratings-outer">
            <div class="ratings-inner">
                <span class="ratings regular-12">Ratings: ${book.ratings.toFixed(
                  1
                )}</span>
                <span class="book-card-star">${renderBookRating(
                  book.ratings
                )}</span>
            </div>
            <div class="vertical-divider"></div>
            <div class="shape-numbers-outer">
                <div class="shape-container">
                    <span class="shape people"><img src="https://res.cloudinary.com/rock-zion/image/upload/v1619690541/Korapay/followers.svg" alt="followers icon" /></span>
                    <span class="number regular-12">${book.tagged}</span>
                </div>
                <div class="shape-container">
                    <span class="shape likes"><img src="https://res.cloudinary.com/rock-zion/image/upload/v1619690541/Korapay/likes.svg" alt="likes icon" /></span>
                    <span class="number regular-12">${book.likes}</span>
                </div>
            </div>
        </div>
    </div>
</div>`;
}

export function renderBookTitle(book) {
  return `<div id="search-suggestion" class="search-suggestion regular-14">${book.book_title} - ${book.authors}</div>`;
}

function renderBookRating(rating) {
  let starImages = "";
  for (let i = 0; i < 5; i++) {
    if (i >= rating) {
      starImages += `<img src="https://res.cloudinary.com/rock-zion/image/upload/v1619690541/Korapay/inactivestar.svg" alt="active star"/>`;
    } else {
      starImages += `<img src="https://res.cloudinary.com/rock-zion/image/upload/v1619690539/Korapay/activestar.svg" alt="active star"/>`;
    }
  }
  return starImages;
}
