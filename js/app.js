import { allBooksArray, recentlyAdded, featuredBooks } from "./data.js";
import {
  renderBookCard,
  renderCarouselCells,
  renderBookTitle,
} from "./htmlcontent";
import Flickity from "flickity";

let model = {
  _allBooks: null,
  _recentlyAdded: null,
  _featuredBooks: null,
};

//controller for manipulating data
let controller = {
  // initialize controller
  init: function () {
    model._allBooks = allBooksArray;
    model._recentlyAdded = recentlyAdded;
    model._featuredBooks = featuredBooks;

    view.renderAllBooks(model._allBooks);
    view.renderRecentlyAdded(model._recentlyAdded);
    view.renderFeaturedBooks(model._featuredBooks);
  },

  getSearchMatches: function (e) {
    let featured = model._featuredBooks;
    let value = e.target.value;
    let results = [];
    results = featured.filter((x) =>
      x.book_title.toLowerCase().includes(value.toLowerCase())
    );
    view.renderSearchResults(results);
  },
};

//view for displaying data
let view = {
  mobileSearchButton: document.querySelector("#search-btn-mobile"),
  allBookscontainer: document.querySelector("#all-books"),
  searchBar: document.querySelector("#search"),
  recentlyAddedContainer: document.querySelector("#recently-added"),
  mainCarousel: document.querySelector("#featured-books"),
  searchPane: document.querySelector("#search-pane"),
  headerBackArrow: document.querySelector("#header-back-arrow"),
  leftHeader: document.querySelector("#left-header-mobile"),
  nav: document.querySelector("#nav-container"),
  hamburger: document.querySelector("#hamburger"),
  sidebar: document.querySelector("#sidebar"),
  siderBackArrow: document.querySelector("#back-arrow-sidebar"),
  elem: document.querySelectorAll(".main-carousel"),
  searchBtn: document.getElementById("search-btn-search"),
  searchBtnMobile: document.getElementById("search-btn-mobile"),

  init: function () {
    this.flickitySlider = document.querySelector(".flickity-slider");
    this.carouselSeeMore = document.querySelectorAll(
      ".carousel-more-container"
    );
    this.carouselClose = document.querySelectorAll(".close-overlay");
    //add eventlistener to search
    view.searchBar.addEventListener("focus", this.openResultPane);
    view.searchBar.addEventListener("focusout", this.closeResultPane);

    //add eventlistener to search btn
    view.mobileSearchButton.addEventListener("click", this.displaySearch);
    view.headerBackArrow.addEventListener("click", this.removeSearch);
    view.hamburger.addEventListener("click", this.openSidebar);
    view.siderBackArrow.addEventListener("click", this.closeSidebar);

    view.searchBar.addEventListener("keyup", controller.getSearchMatches);
    view.searchBtn.addEventListener("click", controller.filterAllBooks);
    view.searchBtnMobile.addEventListener("click", controller.filterAllBooks);

    this.setPlaceHolder();
    this.showMoreDetails();
    this.closeMoreDetails();
  },

  //render search results
  renderSearchResults: function (filteredBooks) {
    console.log("zion");
    view.searchPane.innerHTML = filteredBooks
      .map((book) => {
        return renderBookTitle(book);
      })
      .join("");

    let searchSuggestions = document.getElementsByClassName(
      "search-suggestion"
    );
    for (let i = 0; i < searchSuggestions.length; i++) {
      searchSuggestions[i].addEventListener("click", function () {
        let suggestedText = searchSuggestions[i].textContent.split(" -");
        view.searchBar.value = suggestedText[0];
      });
    }
  },

  showMoreDetails: function () {
    let seeMoreMenu = this.carouselSeeMore;
    for (let i = 0; i < seeMoreMenu.length; i++) {
      seeMoreMenu[i].addEventListener("click", function () {
        seeMoreMenu[i].parentNode.children[2].classList.add(
          "carousel-cell-show-details"
        );
      });
    }
  },

  closeMoreDetails: function () {
    let overlay = document.querySelectorAll(".cell-details-overlay");
    console.log(overlay);
    for (let i = 0; i < overlay.length; i++) {
      overlay[i].addEventListener("click", function () {
        console.log(i);
        overlay[i].parentNode.children[2].classList.remove(
          "carousel-cell-show-details"
        );
      });
    }
  },

  closeSidebar: function () {
    view.sidebar.classList.remove("open-sidebar");
  },

  openSidebar: function () {
    view.sidebar.classList.add("open-sidebar");
  },

  setPlaceHolder: function () {
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      view.searchBar.placeholder = "Search";
    }
  },

  removeSearch: function () {
    document.querySelector("#header-back-arrow").style.display = "none";
    document.querySelector("#search-btn-search").style.display = "none";
    view.nav.classList.remove("left-header-mobile-display-none");
    view.leftHeader.classList.remove("left-header-mobile-display-none");
    view.searchBar.id = "search";
    view.searchBar.value = "";
  },

  displaySearch: function () {
    document.querySelector("#header-back-arrow").style.display = "block";
    document.querySelector("#search-btn-search").style.display = "block";
    view.nav.classList.add("left-header-mobile-display-none");
    view.leftHeader.classList.add("left-header-mobile-display-none");
    view.searchBar.id = "showSearch";
  },

  // function to open results pane
  openResultPane: function () {
    view.searchPane.classList.add("expand-pane");
  },
  closeResultPane: function () {
    view.searchPane.classList.remove("expand-pane");
  },

  renderFeaturedBooks: function (featuredBooks) {
    view.mainCarousel.innerHTML = featuredBooks
      .map((featured) => {
        return renderCarouselCells(featured);
      })
      .join("");
  },

  //function will render all books
  renderAllBooks: function (allBooksArray) {
    view.allBookscontainer.innerHTML = allBooksArray
      .map((book) => {
        return renderBookCard(book);
      })
      .join("");
  },

  //function will render all recently added books
  renderRecentlyAdded: function (recentlyAddedArray) {
    view.recentlyAddedContainer.innerHTML = recentlyAddedArray
      .map((book) => {
        return renderBookCard(book);
      })
      .join("");
  },
};

controller.init();
view.init();

export let flkty = new Flickity(view.elem[0], {
  cellAlign: "left",
  wrapAround: true,
  arrowShape: {
    x0: 10,
    x1: 70,
    y1: 40,
    x2: 70,
    y2: 40,
    x3: 70,
  },
});
