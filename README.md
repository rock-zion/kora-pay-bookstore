# Kora Pay Bookstore 

---

### Objective

Implement a dashboard view for the fictional Korapay Book Club web application, exactly as detailed in the UI mockups provided using HTML, CSS/SCSS, vanilla JavaScript, and only the suggested libraries/plugins for certain UI elements.

### System Requirements
- Node.js 10.13 or later
- MacOS, Windows (including WSL), and Linux are supported


### Installation
To run this project locally, you need to install all dependencies. This can be done by navigating to the project root in your terminal and running the command below:

```sh
npm run install
```
### Run Locally for Dev
After successful installation of the dependencies, you can then run the development server:

```bash
npm run dev
```

> Open [http://localhost:1234](http://localhost:1234) with your browser to see the result.


## Project Structure
This projects follows the a basic vanillajs project with the index.html as 
the base point of entry into the web app, a style folder and and a javascript folder.

Some of the libraries used and their functionalities are listed below:
1. sass: for scss modules transpilation in the project.
2. flickity: a light weight, no dependency JavaScript library for rendering carousels
